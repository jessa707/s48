
let posts = [];

let count = 1;


const showPosts = (posts) => {
	
	
	let post_entries = ''

	posts.forEach((post)=> {
		post_entries += `
			<div id="post-${post.id}">
			    <h3 id="post-title-${post.id}">${post.title}</h3>
			    <p id="post-body-${post.id}">${post.body}</p>
			    <button onclick="editPost('${post.id}')">Edit</button>
			    <button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});


	document.querySelector('#div-post-entries').innerHTML = post_entries
}


// Add new posts
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault()

	

	posts.push({
		id: count, 
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	
	count++


	
	showPosts(posts);
	alert('Successfully added a new Post!');
})



const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}


document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault();

    for (let i = 0; i < posts.length; i++) {
       

        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;
    
            showPosts(posts);
            alert('Successfully updated.');
            
            break;
        }
    }
});

// Deleting Post
const deletePost = (id) => {
	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() == id) {
			posts.splice(i,1);
		}
	}
	showPosts(posts);
	
	alert('The Post was successfully deleted!');
};


// const deletePost = (id) => {
// 	posts = posts.filter((post) => {
// 		if (post.id.toString() !== id) {
// 			return post;
// 		}
// 	});
// 	document.querySelector(`#post-${id}`).remove();
// }




